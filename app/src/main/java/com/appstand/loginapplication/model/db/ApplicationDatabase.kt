package com.appstand.loginapplication.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.appstand.loginapplication.model.db.dto.UserDto
import com.appstand.loginapplication.model.db.dao.UserDao

@Database(entities = arrayOf(UserDto::class), version = 1)
abstract class ApplicationDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}