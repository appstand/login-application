package com.appstand.loginapplication.model.domain

data class User(
    val email: String
)